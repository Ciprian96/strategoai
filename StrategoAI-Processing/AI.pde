class AI{
  //Get a score for each gamestate
  float gamestate(){
    float botPower = 0;
    int countBot = 0;
    float playerPower = 0;
    int countPlayer = 0;
    for(int i = 0; i < 40; i++){
      if(enemy[i].isAlive() && player[i].isAlive())
      {
      }else if(enemy[i].isAlive()){
        botPower += enemy[i].value;
        countBot++;
      }else if(player[i].isAlive()){
        playerPower += player[i].value;
        countPlayer++;
      }       
    }
    botPower = botPower / countBot;
    playerPower = playerPower / countPlayer;
    
    return botPower - playerPower;
  }
  //Recalculate piece values after each move
  float[] recalculatePieceValues(){
      float[] Value = new float[12];
      float factor_rank_difference = 1.55;
      Value[0] = 2;
      Value[1] = 0.02;
      for(int rank = 2; rank <= 10; rank++){
         if(((frequency[rank-1] - deadPCounter[rank-1])+(frequency[rank] - deadPCounter[rank]) > 0) && 
         ((frequency[rank-1] - deadECounter[rank-1])+(frequency[rank] - deadECounter[rank]) > 0)){
            Value[rank] = Value[rank-1] * factor_rank_difference; 
         }else{
            Value[rank] = Value[rank-1];
         }
      }
      Value[1] = Value[10] / 2;
      Value[11] = 0.5;
      return Value;
  }
  
  
  //Get all pieces that can move this turn
  ArrayList<Move> getMoves(){
       ArrayList<Move> moves = new ArrayList<Move>();

        for (int i = 1; i < 34; i++) {
            if(enemy[i].isAlive()){
                if (findPossibleMoves(enemy[i]) != null) {
                    for(int j = 0; j < findPossibleMoves(enemy[i]).size(); j++){
                      moves.add(findPossibleMoves(enemy[i]).get(j));
                    }
                }
            }
        }
        for(int i = 0 ;i < moves.size(); i++){
          Move move = moves.get(i);
          println("Piece: ", move.getPiece().getValue());
        }
        return moves;
   }
   //Find all posiible moves for one piece
   ArrayList<Move> findPossibleMoves(Piece piece){
      final ArrayList<Move> moves = new ArrayList<Move>();
      int row = piece.getBoardRow();
      int col = piece.getBoardCol();
      if(piece.value == 2){
        int up = row-1;
        while(up >= 0){
           if((board[up][col].getOwner() == 0) && 
               board[up][col].getType() != "Water"){
               moves.add(new Move(piece, piece.getCell(), board[up][col]));
           }else if(board[up][col].getOwner() == 1){
                moves.add(new Move(piece, piece.getCell(), board[up][col]));
                up = -1;
           }else{
                up = -1;
           }
           up--;
        }
        int down = row+1;
        while(down < 10){
           if((board[down][col].getOwner() == 0) && 
               board[down][col].getType() != "Water"){
                moves.add(new Move(piece, piece.getCell(), board[down][col]));
           }else if(board[down][col].getOwner() == 1){
                moves.add(new Move(piece, piece.getCell(), board[down][col]));
                down = 10;
           }else{
                down = 10;
           }
           down++;
        }
        int left = col-1;
        while(left >= 0){
           if((board[row][left].getOwner() == 0) && 
               board[row][left].getType() != "Water"){
               moves.add(new Move(piece, piece.getCell(), board[row][left]));
           }else if(board[row][left].getOwner() == 1){
                moves.add(new Move(piece, piece.getCell(), board[row][left]));
                left = -1;
           }else{
                left = -1;
           }
           left--;
        }
        int right = col+1;
        while(right < 10){
           if((board[row][right].getOwner() == 0) && 
               board[row][right].getType() != "Water"){
                moves.add(new Move(piece, piece.getCell(), board[row][right]));
           }else if(board[row][right].getOwner() == 1){
                moves.add(new Move(piece, piece.getCell(), board[row][right]));
                right = 10;
           }else{
                right = 10;
           }
           right++;
        }
      }
          if(row + 1 < 10){
            if((board[row+1][col].getOwner() == 1 || 
               board[row+1][col].getOwner() == 0) && 
               board[row+1][col].getType() != "Water"){
                moves.add(new Move(piece, piece.getCell(), board[row+1][col]));
            }
          }
          if(row - 1 >= 0){ 
            if((board[row-1][col].getOwner() == 1 || 
               board[row-1][col].getOwner() == 0) && 
                board[row-1][col].getType() != "Water"){
                  moves.add(new Move(piece, piece.getCell(), board[row-1][col]));
            }
          }
          
          if(col + 1 < 10 && (board[row][col+1].getOwner() == 1 || 
               board[row][col+1].getOwner() == 0 ) && 
              board[row][col+1].getType() != "Water"){
                moves.add(new Move(piece, piece.getCell(), board[row][col+1]));
          }
          if(col - 1 >= 0 && (board[row][col-1].getOwner() == 1 || 
               board[row][col-1].getOwner() == 0 )&& 
              board[row][col-1].getType() != "Water"){
                moves.add(new Move(piece, piece.getCell(), board[row][col-1]));
          }
          return moves;
    }
}
