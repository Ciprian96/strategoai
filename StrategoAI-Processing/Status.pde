public enum Status
{
  START,
  SETUP,
  PLAYING,
  STOPPED
}
