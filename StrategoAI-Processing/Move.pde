class Move{  //<>//
  Piece piece;
  Cell from;
  Cell to;
  Plan plan = new Plan();
  
  Move(){
   piece = null;
   from = null;
   to = null;
  }
  
  Move(Piece piece, Cell from, Cell to){
    this.piece = piece;
    this.from = from;
    this.to = to;    
  }
  
  Piece getPiece(){
     return piece; 
  }
  
  Cell getFrom(){
    return from;
  }
  
  Cell getTo(){
    return to;
  }
  
  void doMove(Piece piece, Cell from, Cell to){
      from.setPiece(new Piece());
      piece.getCell().setOwner(0);
      to.setPiece(piece);
      to.setOwner(piece.getOwner());
      piece.setXpos(to.getXpos()+5);
      piece.setYpos(to.getYpos()+5);
      piece.setCell(to);
  }
  
  void doMove(){
    from.setPiece(new Piece());
    from.setOwner(0);
    piece.getCell().setOwner(0);
    to.setPiece(piece);
    to.setOwner(piece.getOwner());
    piece.setXpos(to.getXpos()+5);
    piece.setYpos(to.getYpos()+5);
    piece.setCell(to);
  }
 
  
  void enemyMove(){
     for(int row = 0; row < 10; row++){
        for(int col = 0 ; col < 10; col++){  
         print(board[row][col].getPiece().getValue());  
    }
      println();
  }
    
     ArrayList<Move> moves = ai.getMoves();
     Move move = moves.get(0);
     Move Bmove;
     float maxScore = 0;
     float score;
     //Calculate score for each possible move
     for(int i = 0; i < moves.size(); i++){
        Bmove = moves.get(i);
        score = plan.ImmediateCapturePlan(moves.get(i)) + plan.TacticalDefensePlan(moves.get(i)) + plan.TacticalAttackPlan(moves.get(i)) + plan.DestroyBombsPlan(moves.get(i)) + 
        plan.DefendTheMarshalFromSpy(moves.get(i)) + plan.RandomPlan(moves.get(i)) + plan.MovePenaltyPlan(moves.get(i)) + plan.StrategicDefensePlan(moves.get(i)) + 
        plan.StrategicAttackPlan(moves.get(i)) + plan.twoSquareRule(moves.get(i));
        //Get the move with highest score
        if(score >= maxScore){
          maxScore = score;
          move = moves.get(i);
        }     
     }
     Piece player; //<>//
     if(move.getTo().getOwner() == 1){ //<>//
      player = move.getTo().getPiece(); //<>//
      move.getPiece().attack(player); //<>//
      if(move.getPiece().isAlive()){ //<>//
        move.doMove(player, player.getCell(), playerGraveyard[player.getValue()]); //<>//
        deadPCounter[player.getValue()]++;
        move.doMove(); //<>//
      }else{ //<>//
        move.doMove(move.getPiece(), move.getPiece().getCell(), enemyGraveyard[move.getPiece().getValue()]);  //<>//
        deadECounter[move.getPiece().getValue()]++;
      } //<>//
      }else{ 
        move.doMove(); 
      }
      move.getPiece().setMoved(true);
      last3EMoves.add(move);
  }
  
  void playerMove(){
    Piece enemy;
    if(to.getOwner() == 2){
      enemy = to.getPiece();
      piece.attack(enemy);
      if(piece.isAlive()){
        doMove(enemy, enemy.getCell(), enemyGraveyard[enemy.getValue()]);
        doMove(piece, from, to);
        deadECounter[enemy.getValue()]++;
      }else{
        doMove(piece, piece.getCell(), playerGraveyard[piece.getValue()]); 
        deadPCounter[piece.getValue()]++;
      }
    }else{
      doMove(piece, from, to);
      if(piece.getValue() == 2 && (to.getColPos() > from.getColPos() + 1 || to.getColPos() > from.getColPos() - 1 || to.getRowPos() > from.getRowPos() + 1 || to.getRowPos() < from.getRowPos() - 1))
        piece.setRevealed(true);
    }
    piece.setSelected(false);
    piece.setMoved(true);
    moveMade = true;
 }
 
 public boolean isNull()
    {
        return piece == null;
    }
  

}
