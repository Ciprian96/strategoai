  class Piece {
    color c;
    float xpos;
    float ypos; 
    float points;
    int boardRow;
    int boardCol;
    int value;
    boolean revealed;
    boolean alive;
    boolean overPiece;
    boolean selected;
    boolean moved;
    int owner;
    Cell cell;
    Cell[] possibleMoves = new Cell[4];
    
    Piece(){
      owner = 0;  
    }
    
    Piece(Cell tempCell, int tempValue, int tempOwner){
      xpos = tempCell.getXpos()+5;
      ypos = tempCell.getYpos()+5;
      tempCell.setOwner(tempOwner);
      tempCell.setPiece(this);
      cell = tempCell;
      value = tempValue;
      owner = tempOwner;
      alive = true;
      moved = false;
    }
    
    void draw(){
       if(owner == 1)
         c = color(200,0,0);
       else 
         c = color(0,100,100);
       fill(c);
       if(selected == true){
         rect(xpos-5,ypos-5,50,50);
         fill(0);
         if(value != 11 && value != 0){
           textSize(40);
           if(value == 10){
             text(value,xpos-5,ypos+35);
           }else{
             text(value,xpos+8,ypos+35);
           }
         }else if(value == 11){
           ellipse(xpos+20,ypos+25, 23, 23);
           strokeWeight(4);
           line(xpos+20,ypos+16, xpos+25, ypos+8);
         }else if(value == 0){
           strokeWeight(4);
           line(xpos+10, ypos+35, xpos+10, ypos+5);
           rect(xpos+10, ypos+5, 25,15);
         }
       }else{
       strokeWeight(2);
       rect(xpos,ypos,40,40);
       fill(0);
       if(revealed && owner == 2){
         if(value != 11 && value != 0){
           if(value == 10){
             text(value,xpos,ypos+30);
           }else{
             textSize(33);
             text(value,xpos+9,ypos+30);
           }
         }else if(value == 11){
           ellipse(xpos+20,ypos+25, 19, 19);
           strokeWeight(3);
           line(xpos+20,ypos+16, xpos+25, ypos+10);
         }else if(value == 0){
           strokeWeight(3);
           line(xpos+10, ypos+30, xpos+10, ypos+10);
           rect(xpos+10, ypos+5, 20,10);
         }
         
       }else if(owner == 1){
         if(value != 11 && value != 0){
           if(value == 10){
             textSize(33);
             text(value,xpos,ypos+30);
           }else{
             textSize(33);
             text(value,xpos+9,ypos+30);
           }
         }else if(value == 11){
           ellipse(xpos+20,ypos+25, 19, 19);
           strokeWeight(3);
           line(xpos+20,ypos+16, xpos+25, ypos+10);
         }else if(value == 0){
           strokeWeight(3);
           line(xpos+10, ypos+30, xpos+10, ypos+10);
           rect(xpos+10, ypos+5, 20,10);
         }
       }
       
       }
    }
    
    void setXpos(float bx){
      xpos = bx;
    }
    
    void setYpos(float by){
      ypos = by;
    }
    
    void setOverPiece(boolean op){
       overPiece = op; 
    }
    
    void setPoints(float currentPoints){
      points = currentPoints;
    }
    
    float getPoints(){
      return points;
    }
    
    boolean getOverPiece(){
      return overPiece;
    }
    
    void setSelected(boolean sel){
       selected = sel; 
    }
    
    boolean isSelected(){
      return selected;
    }
    
    void setRevealed(boolean tempR){
      revealed = tempR;
    }
    
    boolean isRevealed(){
      return revealed; 
    }
    
    float getXpos(){
      return xpos; 
    }
    
    float getYpos(){
      return ypos; 
    }
    
    int getOwner(){
      return owner; 
    }
    
    int getBoardRow(){
      return cell.getRowPos(); 
    }
    int getBoardCol(){
      return cell.getColPos(); 
    }
    
    int getValue(){
      return value;
    }
    
    boolean isAlive(){
      return alive; 
    }
    
    boolean isMoved(){
      return moved; 
    }
    
    void setMoved(boolean tempMoved){
      moved = tempMoved;
    }
    
    Cell getCell(){
      return cell;
    }
    
    void setCell(Cell cell){
      this.cell = cell;
    }
    
    void attack(Piece otherPiece){
        revealed = true;
        otherPiece.setRevealed(true);
        if(value >= otherPiece.getValue()){
          otherPiece.alive = false;
        }else if(value == 3 && otherPiece.getValue()==11){
          otherPiece.alive = false;
        }else if(value == 1 && otherPiece.getValue()==10){
          otherPiece.alive = false;
        }else{
          alive = false;
        }
    }
  }
