class Plan{
  //Plan to capture an enemy piece
  float ImmediateCapturePlan(Move move){
   if(move.getTo().getPiece().getOwner() == 1){
     Piece enemy = move.getTo().getPiece();
     Piece bot   = move.getPiece();
     if(enemy.isRevealed()){
       if(enemy.getValue() < bot.getValue() || (enemy.getValue() == 11 && bot.getValue() == 3) || (enemy.getValue() == 10 && bot.getValue() == 1)){
          if(bot.isRevealed()) return  enemy.getPoints() * 200;
          if(!bot.isRevealed()) return enemy.getPoints() * 200 * (bot.getPoints()*25/100);
       }else if(enemy.getValue() == bot.getValue()){
         return  enemy.getPoints()*300;
       }else{
         return -100;
       }
     }else if(!enemy.isRevealed()){
         if(bot.getValue() == 2){
            return 5; 
         }else{
            float power = ai.gamestate();
            if(power > 1)
              return bot.getPoints() * 100 + power * 10;
         }
     }
    } 
    return 0;
  }
  
  float TacticalDefensePlan(Move move){
    Piece bot = move.getPiece();
    if(bot.getValue() == 10)
      return 0;
    int toRow = move.getTo().getRowPos();
    int toCol = move.getTo().getColPos();
    int fromRow = move.getFrom().getRowPos();
    int fromCol = move.getFrom().getColPos();
    float score = 0;
    float maxScore = 0;
    ArrayList<Cell> toNeighbours = new ArrayList<Cell>();
    if(toRow != 0)
      toNeighbours.add(board[toRow-1][toCol]);
    if(toRow != 9)
      toNeighbours.add(board[toRow+1][toCol]);
    if(toCol != 0)
      toNeighbours.add(board[toRow][toCol-1]);
    if(toCol != 9)
      toNeighbours.add(board[toRow][toCol+1]);
    for(int i = 0; i < toNeighbours.size(); i++){
      Cell neighbour = toNeighbours.get(i);
      Piece enemy = neighbour.getPiece();
      if(enemy.getOwner() == 1 && enemy.isRevealed() && enemy.getValue() > bot.getValue())
        return -(bot.getPoints()*100);
    }
    if(bot.isMoved()){
      int i = 3;
      while(i < 34){
        if(player[i].getValue() <= bot.getValue()){
          i = i + frequency[player[i].getValue()]; 
        }else{
          if(player[i].isAlive()){
            if(player[i].isRevealed()){
              if(player[i].getBoardCol() == fromCol){
                if(toCol == fromCol + 1){
                  score = bot.getPoints()*30;
                }else if(toCol == fromCol - 1){
                  score = bot.getPoints()*30;
                }
              }else if(player[i].getBoardRow() == fromRow){
                  if(toRow == fromRow + 1){
                    score =  bot.getPoints()*30;             
                  }else if(toRow == fromRow - 1){
                    score =   bot.getPoints()*30;
                  }
              }
            }
          }
          if(score > maxScore)
            maxScore = score;
          i++;
        }
      }
    }
    return maxScore;
  }
  
  float TacticalAttackPlan(Move move){
    Piece bot   = move.getPiece();
    int toRow = move.getTo().getRowPos();
    int toCol = move.getTo().getColPos();
    int fromRow = move.getFrom().getRowPos();
    int fromCol = move.getFrom().getColPos();
    float score = 0;
    float maxScore = 0;
    int i = 0;
    while(player[i].getValue() < bot.getValue() || (player[i].getValue()==10 && bot.getValue() == 1)){
      score = 0;
      if(player[i].isAlive()){
          if(player[i].isRevealed()){
            if(player[i].getBoardCol() < fromCol){
              if(toCol == fromCol - 1){
                score = -((player[i].getBoardCol() - fromCol) * bot.getPoints() * 30);
              }
            }else if(player[i].getBoardCol() > fromCol){
              if(toCol == fromCol + 1){
                score = ((player[i].getBoardCol() - fromCol) * bot.getPoints() * 30);
              }
            }else{
              if(player[i].getBoardRow() < fromRow){
                if(toRow == fromRow - 1){
                  score = -((player[i].getBoardRow() - fromRow) * bot.getPoints() * 30);             
                }
              }else if(player[i].getBoardRow() > fromRow){
                if(toRow == fromRow + 1){
                  score = ((player[i].getBoardRow() - fromRow) * bot.getPoints() * 30);
                }
              }
            }
            if(score > maxScore)
              maxScore = score;
          }
      }
      i++;
    }
  return maxScore;
  }
  
  float DestroyBombsPlan(Move move){
    Piece bot = move.getPiece();
    int toRow = move.getTo().getRowPos();
    int toCol = move.getTo().getColPos();
    int fromRow = move.getFrom().getRowPos();
    int fromCol = move.getFrom().getColPos();
    float score = 0;
    float maxScore = 0;
    if(bot.getValue() == 3){
      for(int i = 34; i < 40; i++){
        if(player[i].isAlive() && player[i].isRevealed()){
            if(player[i].getBoardCol() < fromCol){
              if(toCol == fromCol - 1){
                score = -((player[i].getBoardCol() - fromCol) * player[i].getPoints() * 40);
              }
            }else if(player[i].getBoardCol() > fromCol){
              if(toCol == fromCol + 1){
                score = ((player[i].getBoardCol() - fromCol) * player[i].getPoints() * 40);
              }
            }else{
              if(player[i].getBoardRow() < fromRow){
                if(toRow == fromRow - 1){
                  score = -((player[i].getBoardRow() - fromRow) * player[i].getPoints() * 60); 
                }
              }else if(player[i].getBoardRow() > fromRow){
                if(toRow == fromRow + 1){
                  score = ((player[i].getBoardRow() - fromRow) * player[i].getPoints() * 60);
                }
              }
            }
            if(score > maxScore)
              maxScore = score;
        }
      }
      return maxScore;
    }
    return 0;
  }
  
  float DefendTheMarshalFromSpy(Move move){
    if(move.getPiece().getValue() == 10){
      if(player[1].isAlive()){
        Piece bot = move.getPiece();
        int toRow = move.getTo().getRowPos();
        int toCol = move.getTo().getColPos();
        ArrayList<Cell> toNeighbours = new ArrayList<Cell>();
        int hiddenEnemies = 0;
        if(toRow != 0)
          toNeighbours.add(board[toRow-1][toCol]);
        if(toRow != 9)
          toNeighbours.add(board[toRow+1][toCol]);
        if(toCol != 0)
          toNeighbours.add(board[toRow][toCol-1]);
        if(toCol != 9)
          toNeighbours.add(board[toRow][toCol+1]);
        
        for(int i = 0; i < toNeighbours.size(); i++){
          Cell neighbour = toNeighbours.get(i);
          if(neighbour.getPiece().getOwner() == 1){
            Piece enemy = neighbour.getPiece();
            if(enemy.isRevealed() && enemy.getValue() == 1){
              return -100;
            }else if(!enemy.isRevealed()){
              hiddenEnemies++;
            }
          }
        }
        int allDeadPieces = 0;
        for(int i = 3; i < player.length; i++){
          if(!player[i].isAlive() || player[i].isRevealed())
            allDeadPieces++;
        }
          if(!player[1].isRevealed())
             if((100 / (40 - allDeadPieces)) * hiddenEnemies < 30) return 0;
             else return -(100 / (40 - allDeadPieces)) * hiddenEnemies;
        }
      }
    return 0;
  }
  
  float StrategicDefensePlan(Move move){
    Piece bot = move.getPiece();
    int toRow = move.getTo().getRowPos();
    int toCol = move.getTo().getColPos();
    int fromCol = move.getFrom().getColPos();
    int fromRow = move.getFrom().getRowPos();
    int i = 0;
    while(enemy[i].getValue() < bot.getValue() && enemy[i].isRevealed()){
      if(enemy[i].getBoardCol() < fromCol){
              if(toCol == fromCol - 1){
                return -((enemy[i].getBoardCol() - fromCol) * bot.getPoints()*10);
              }
            }else if(enemy[i].getBoardCol() > fromCol){
              if(toCol == fromCol + 1){
                return +((enemy[i].getBoardCol() - fromCol) * bot.getPoints()*10);
              }
            }else{
              if(enemy[i].getBoardRow() < fromRow){
                if(toRow == fromRow - 1){
                  return -((enemy[i].getBoardRow() - fromRow) * bot.getPoints()*10);             
                }
              }else if(enemy[i].getBoardRow() > fromRow){
                if(toRow == fromRow + 1){
                  return +((enemy[i].getBoardRow() - fromRow) * bot.getPoints()*10);
                }
              }
            }
      i++;
    }
    return 0;
  }
  
  float StrategicAttackPlan(Move move){
    Piece bot = move.getPiece();
    int toRow = move.getTo().getRowPos();
    int fromRow = move.getFrom().getRowPos();
    if( toRow == fromRow + 1 && toRow <= 5){
      return bot.getPoints()*5; 
    }else if(toRow == fromRow + 1 && toRow > 5){
       return bot.getPoints()*2; 
    }
    return 0;
  }
  
  float RandomPlan(Move move){
    float result = random(0, 1);
    return result ;
  }
  
  float MovePenaltyPlan(Move move){
    Piece bot = move.getPiece();
    int movedCounter = 0;
    for(int i = 0; i < enemy.length; i++){
      if(enemy[i].isAlive()){
        if(enemy[i].isMoved() || enemy[i].isRevealed()){
          movedCounter++;
        }
      }
    }
    if(!bot.isMoved()){
      return -movedCounter/10;
    }
    return 0;
  }
  
  float twoSquareRule(Move move){
    boolean sameRow = true;
    boolean sameCol = true;
    boolean samePiece = true;
    float minY;
    float maxY;
    float minX;
    float maxX;
    float max = 0;
    float min = 400;
    float maximum = 0;
    float minimum = 400;
    if(last3EMoves.size() > 2){
      for(int i = last3EMoves.size() - 1; i >= last3EMoves.size() - 3; i--){
        Move last = last3EMoves.get(i);
        if(last.getPiece() != move.getPiece()){
           samePiece = false;
        }
        if(last.getFrom().getRowPos() != move.getTo().getRowPos()){
           sameRow = false; 
        }
        if(last.getFrom().getColPos() != move.getTo().getColPos()){
           sameCol = false; 
        }
        // Verifica Maximul dintre minY si Minimul dintre maxY
        if(last.getFrom().getColPos() < last.getTo().getColPos()){
          minY = last.getTo().getYpos();
        }else{
          minY = last.getFrom().getYpos(); 
        }
        if(max < minY) 
          max = minY;
          
        if(last.getFrom().getColPos() > last.getTo().getColPos()){
          maxY = last.getTo().getYpos();
        }else{
          maxY = last.getFrom().getYpos(); 
        }
        if(min > maxY) 
          min = maxY;
          
        // Verifica Maximul dintre minX si Minimul dintre maxX
        if(last.getFrom().getRowPos() > last.getTo().getRowPos()){
          minX = last.getTo().getXpos();
        }else{
          minX = last.getFrom().getXpos(); 
        }
        if(maximum < minX) 
          maximum = minX;
          
        if(last.getFrom().getColPos() < last.getTo().getColPos()){
          maxX = last.getTo().getXpos();
        }else{
          maxX = last.getFrom().getXpos(); 
        }
        if(minimum > maxX) 
          minimum = maxX;
      }
      if(samePiece){
        if(sameRow){
          println(min , max);
           if(min >= max){
             last3EMoves.clear();
             return -100;
           }
        }else if(sameCol){
          println(minimum, maximum);
           if(minimum >= maximum){
             last3EMoves.clear();
             return -100;
           }
        }
      }
    }
    return 0; 
  }
}
