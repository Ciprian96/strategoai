class Setup{
  Cell[][] board = new Cell[10][10];
  Piece currentPiece;
  Cell currentCell;
  
  Setup(Cell[][] tempBoard){
    for(int i = 0; i < board.length; i++){
      for(int j = 0; j < board[i].length; j++){
        board[i][j] = tempBoard[i][j];
      }
    }
  }
  
  void setCurrentPiece(Piece tempCurrentPiece){
    currentPiece = tempCurrentPiece;
  }
  
  void setCurrentCell(Cell tempCurrentCell){
    currentCell = tempCurrentCell;
  }
  
  Piece getCurrentPiece(){
    return currentPiece;
  }
  
  Cell getCurrentCell(){
    return currentCell;
  }
  
  void enemySetup(Piece[] piece){
      int firstRow;
      int secondRow;
      int thirdRow;
      int fourthRow;
      int randomRow;
      int randomCol;
      int bomb = 0;
      if(piece[0].getOwner() == 2){
        firstRow = 0;
        secondRow = 1;
        thirdRow = 2;
        fourthRow = 3;
        
      }else{
        firstRow = 9;
        secondRow = 8;
        thirdRow = 7;
        fourthRow = 6;
      }
      Cell[]  bombCell = new Cell[6];
      for(int i = 0; i < enemy.length; i++){
          if(piece[i].getValue() == 0){
               randomCol = (int) random(1,9); //<>//
               move = new Move(piece[i], piece[i].getCell(), board[firstRow][randomCol]);
               move.doMove();
               board[secondRow][randomCol].setOwner(piece[0].getOwner());
               bombCell[0] = board[secondRow][randomCol];
               board[firstRow][randomCol + 1].setOwner(piece[0].getOwner());
               bombCell[1] = board[firstRow][randomCol + 1];
               board[firstRow][randomCol - 1].setOwner(piece[0].getOwner());
               bombCell[2] = board[firstRow][randomCol - 1];
               for(int b = 3; b <= 5; b++){
                 do{
                   if(piece[0].getOwner() == 2){
                      randomRow = (int) random(firstRow, thirdRow+1);
                    }else{
                      randomRow = (int) random(thirdRow, firstRow+1);
                    }
                   randomCol = (int) random(10);
                 }while(board[randomRow][randomCol].getOwner() == piece[0].getOwner());
                 board[randomRow][randomCol].setOwner(piece[0].getOwner());
                 bombCell[b] = board[randomRow][randomCol];
               }
        }else if(piece[i].getValue() == 11){
            move = new Move(piece[i], piece[i].getCell(), bombCell[bomb]);
            move.doMove();
            bomb++;
        }else{
         do{
           if(piece[0].getOwner() == 2){
                  randomRow = (int) random(firstRow, fourthRow+1);
                }else{
                  randomRow = (int) random(fourthRow, firstRow+1);
                }
           randomCol = (int) random(10);
         }while(board[randomRow][randomCol].getOwner() == piece[0].getOwner());
           move = new Move(piece[i], piece[i].getCell(), board[randomRow][randomCol]);
           move.doMove();
         
        }

      }
      
    }
  
  void playerSetup(){
     
  }
  
  boolean playerSetupFinished(){
     for(int i = 6; i < 10; i++){
       for(int j = 0; j < 10; j++){
         if(board[i][j].getOwner() == 0){
           return false;
         }
       }
     }
     return true;
  }
  
}
