Piece player[] = new Piece[40];
Piece enemy[] = new Piece[40];
Piece current = player[0];
Cell currentCell = null;
Setup setup;
Move move;
AI ai = new AI();
Cell[][] board = new Cell[10][10];
Cell[] enemyGraveyard = new Cell[12];
Cell[] playerGraveyard = new Cell[12];
Cell auto;
Status status = Status.START;
boolean overPiece = false;
boolean moveMade = false;
int count = 0;
int[] pieces    = {0,1,2,3,4,5,6,7,8,9,10,11};
int[] frequency = {1,1,8,5,4,4,4,3,2,1,1,6};
float time = millis();
int[] deadPCounter = new int[12];
int[] deadECounter = new int[12];
ArrayList<Move> last3PMoves = new ArrayList<Move>();
ArrayList<Move> last3EMoves = new ArrayList<Move>();

void setup() {
  size(840, 640);
}
void draw(){
  background(150);
  for(int i = 0; i < enemyGraveyard.length; i++){
    enemyGraveyard[i] = new Cell("GraveYard", 650,50*(i));
    textSize(36);
    fill(0);
    text(deadECounter[i],610,enemyGraveyard[i].getYpos()+40);
    enemyGraveyard[i].draw();
    playerGraveyard[i] = new Cell("GraveYard", 700,50*i);
    fill(0);
    text(deadPCounter[i],770,enemyGraveyard[i].getYpos()+40);
    playerGraveyard[i].draw();
  }
  if(status == Status.START){
    StrategoBoard();
  }
  
  if(status == Status.SETUP){
    auto = new Cell("Setup", 50  , 560);
    auto.draw();
  }
  for(int row = 0; row < board.length; row++){
    for(int col = 0 ; col < board[row].length; col++){  
      board[row][col].draw();    
    }
  }
  
  if(status == Status.SETUP && setup.playerSetupFinished() == true){
    setup.enemySetup(enemy);
    status = Status.PLAYING;
  }
  
  
   for(int i = 0; i < enemy.length; i++){
          enemy[i].draw();
          enemy[i].setPoints(ai.recalculatePieceValues()[enemy[i].getValue()]);
          if(player[i] != current)
            player[i].setSelected(false);
          player[i].draw();
          player[i].setPoints(ai.recalculatePieceValues()[player[i].getValue()]);
      }
   if(status == Status.PLAYING){
     if(moveMade){
       float wait = 1000;
       if(millis() > time + wait){
         for(int i = 0; i < 40; i++){
           if(enemy[i].isAlive())
             enemy[i].setRevealed(false);  
         }
         move.enemyMove();
         moveMade = false;         
       }
       for(int row = 0; row < 10; row++){
        for(int col = 0 ; col < 10; col++){  
          board[row][col].setPossibleMove(false);
        }
       }
     }
     int bBombs = 0;
     int pBombs = 0;
     for(int bomb = 10; bomb < 15; bomb++){
        if(!enemy[bomb].isAlive()){
          bBombs++; 
        }
        if(!player[bomb].isAlive()){
          pBombs++;
        }
     }
     if(!player[0].isAlive() || (pBombs == 5 && board[enemy[0].getBoardRow()+1][enemy[0].getBoardCol()].getPiece().getValue() == 11 &&
         board[enemy[0].getBoardRow()][enemy[0].getBoardCol()-1].getPiece().getValue() == 11 && board[enemy[0].getBoardRow()][enemy[0].getBoardCol()+1].getPiece().getValue() == 11)){
       status = Status.STOPPED;
       fill(0,0,255);
       textSize(100);
       translate(width/2, height/2);
       text("DEFEAT", -200, 0);
       noLoop();
     }else if(!enemy[0].isAlive() || (pBombs == 5 && board[player[0].getBoardRow()-1][player[0].getBoardCol()].getPiece().getValue() == 11 &&
         board[player[0].getBoardRow()][player[0].getBoardCol()-1].getPiece().getValue() == 11 && board[player[0].getBoardRow()][player[0].getBoardCol()+1].getPiece().getValue() == 11)){
       status = Status.STOPPED;
       fill(255,0, 0);
       textSize(100);
       translate(width/2, height/2);
       text("VICTORY", -200, 0); 
       noLoop();
     }
   }
}
// Create Stratego board
void StrategoBoard(){
  stroke(0);
  for(int row = 0; row < 10; row++){
    for(int col = 0 ; col < 10; col++){
        if(((col > 1 && col < 4) || (col > 5 && col < 8)) && (row > 3 && row < 6) ){
           board[row][col] = new Cell("Water",50*(col+1),50*(row+1),row,col);
           board[row][col].setOwner(0);
        }
        else{    
          board[row][col] = new Cell("Ground",50*(col+1),(row+1)*50,row,col);
          board[row][col].setOwner(0);
        }
    }
  }
  
  //Create pieces
  for(int owner = 1; owner <= 2; owner++){
     for(int i = 0; i < pieces.length; i++ ){
        for(int j = 0; j < frequency[i]; j++){
           if(owner == 1){
                    player[count] = new Piece(playerGraveyard[i],pieces[i],owner);
           }
           if(owner == 2){
                 enemy[count] = new Piece(enemyGraveyard[i],pieces[i],owner);                    
    
           }
           count++;
           if(count == 40 )
             count = 0;
             
        }
     }
  }
  setup = new Setup(board);
  status = Status.SETUP;
}

void mousePressed(){
  moveMade = false;
  MouseOverPiece();
  // If the game started select a piece when you click it
  if(status == Status.PLAYING){
    if(current!=null && current.getOverPiece() && current.getValue()!= 0 && current.getValue()!= 11)
        current.setSelected(true);
    
    if(current!=null && current.isSelected()){
        findPossibleMove(current);
        for(int i = 0; i < 10; i++){
          for(int j = 0; j < 10; j++){
            if(board[i][j].isPossibleMove()){
              if(board[i][j].getXpos() < mouseX && mouseX < board[i][j].getXpos()+50 && 
                board[i][j].getYpos() < mouseY && mouseY < board[i][j].getYpos()+50){
                  currentCell = current.getCell();
                  currentCell.setOwner(0);
                  move = new Move(current, currentCell, board[i][j]);
                  move.playerMove();
                  time = millis();
                  redraw();
                  
                }
            }
          }
        }
    }
    
  }else if(status == Status.SETUP){
    if(auto.getXpos() < mouseX && mouseX < auto.getXpos()+50 && 
       auto.getYpos() < mouseY && mouseY < auto.getYpos()+50){
         setup.enemySetup(player);         
    }else{
    if(current != null && current.getOverPiece())
            current.setSelected(true);
    
        if(current != null && current.isSelected()){
        for(int i = 6; i < 10; i++){
          for(int j = 0; j < 10; j++){
             if(board[i][j].getXpos() < mouseX && mouseX < board[i][j].getXpos()+50 && 
                board[i][j].getYpos() < mouseY && mouseY < board[i][j].getYpos()+50){
                  if(board[i][j].getOwner() == 0){
                    currentCell = current.getCell();
                    currentCell.setOwner(0);
                    move = new Move(current, currentCell,board[i][j]);
                    move.doMove();
                    current.setSelected(false);
                  }
             }
          }
        }
       }
    
  }
  }
   redraw();
}

//Find all posiible moves for the selected piece
void findPossibleMove(Piece tempPiece){
      int row = tempPiece.getBoardRow();
      int col = tempPiece.getBoardCol();
      if(tempPiece.value == 2){
        int up = row-1;
        while(up >= 0){
           if((board[up][col].getOwner() == 0) && 
               board[up][col].getType() != "Water"){
                board[up][col].setPossibleMove(true);
           }else if(board[up][col].getOwner() == 2){
                board[up][col].setPossibleMove(true);
                up = -1;
           }else{
                up = -1;
           }
           up--;
        }
        int down = row+1;
        while(down < 10){
           if((board[down][col].getOwner() == 0) && 
               board[down][col].getType() != "Water"){
                board[down][col].setPossibleMove(true);
           }else if(board[down][col].getOwner() == 2){
                board[down][col].setPossibleMove(true);
                down = 10;
           }else{
                down = 10;
           }
           down++;
        }
        int left = col-1;
        while(left >= 0){
           if((board[row][left].getOwner() == 0) && 
               board[row][left].getType() != "Water"){
                board[row][left].setPossibleMove(true);
           }else if(board[row][left].getOwner() == 2){
                board[row][left].setPossibleMove(true);
                left = -1;
           }else{
                left = -1;
           }
           left--;
        }
        int right = col+1;
        while(right < 10){
           if((board[row][right].getOwner() == 0) && 
               board[row][right].getType() != "Water"){
                board[row][right].setPossibleMove(true);
           }else if(board[row][right].getOwner() == 2){
                board[row][right].setPossibleMove(true);
                right = 10;
           }else{
                right = 10;
           }
           right++;
        }
      }
          if(row + 1 < 10){
            if((board[row+1][col].getOwner() == 2 || 
               board[row+1][col].getOwner() == 0) && 
               board[row+1][col].getType() != "Water"){
                board[row+1][col].setPossibleMove(true);
            }
          }
          if(row - 1 >= 0){ 
            if((board[row-1][col].getOwner() == 2 || 
               board[row-1][col].getOwner() == 0) && 
                board[row-1][col].getType() != "Water"){
                  board[row-1][col].setPossibleMove(true);
            }
          }
          
          if(col + 1 < 10 && (board[row][col+1].getOwner() == 2 || 
               board[row][col+1].getOwner() == 0 ) && 
              board[row][col+1].getType() != "Water"){
                board[row][col+1].setPossibleMove(true);
          }
          if(col - 1 >= 0 && (board[row][col-1].getOwner() == 2 || 
               board[row][col-1].getOwner() == 0 )&& 
              board[row][col-1].getType() != "Water"){
                board[row][col-1].setPossibleMove(true);
          }
    }
 //Check if mouse is over a piece
 void MouseOverPiece(){
    for(int i = 0; i < 40; i++){
        if(player[i].getXpos() < mouseX && mouseX < player[i].getXpos()+40 && 
        player[i].getYpos() < mouseY && mouseY < player[i].getYpos()+40){
          player[i].setOverPiece(true);
          current = player[i];
        }else{
           player[i].setOverPiece(false);
        }
    }
 }
 
