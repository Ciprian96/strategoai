class Cell{
  float xpos;
  float ypos;
  int rowPos;
  int colPos;
  int owner;
  boolean possibleMove;
  color c;
  Piece piece;
 
  
  String type;
  
  Cell(){
     owner = 0;
  }
  
  Cell(String tempType,float tempXpos, float tempYpos,int tempRowPos, int tempColPos){
     type = tempType;
     xpos = tempXpos;
     ypos = tempYpos;
     rowPos = tempRowPos;
     colPos = tempColPos;
     owner = 0;
     piece = new Piece();
  }
  
  Cell(String tempType,float tempXpos, float tempYpos){
     type = tempType;
     xpos = tempXpos;
     ypos = tempYpos;
  }
  
  void draw(){
    if(possibleMove == true){
      strokeWeight(4);
      stroke(0);
    }
    else{
      strokeWeight(1);
      stroke(0);
    }
    if(type == "Water")
      c = color(0,0,200);
    else
      c = color(0,200,0);
    if(type == "Setup"){
      textSize(14);
      stroke(0);
      fill(0);
      text("AUTO", 50, 565);
    }
    fill(c);
    strokeWeight(1);
    rect(xpos,ypos,50,50);    
  }
  
  float getXpos(){
    return xpos; 
  }
  
  float getYpos(){
    return ypos; 
  }
  
  int getOwner(){
    return owner; 
  }
  
  void setOwner(int tempOwner){
    owner = tempOwner;
  }
  
  boolean isPossibleMove(){
    return possibleMove; 
  }
  
  void setPossibleMove(boolean tempPM){
    possibleMove = tempPM;
  }
  
  void setPiece(Piece tempPiece){
    piece = tempPiece;
    owner = piece.getOwner();
  }
  
  Piece getPiece(){
    return piece;
  }
  
  String getType(){
    return type;
  }
  
  int getRowPos(){
    return rowPos;
  }
  
  int getColPos(){
    return colPos;
  }
  
}
